<?php
namespace Komponente1\API1;

/**
 * Created by PhpStorm.
 * User: michaelsandritter
 * Date: 3/09/2015
 * Time: 2:55 PM
 */

use Komponente1\API1\Service;

class ServiceTest1 extends \PHPUnit_Framework_TestCase{

    private $service;
    /**
     *
     */
    public function setUp()
    {
        $this->service = new Service();
    }

    /**
     * @test
     */
    public function shouldReturnContent()
    {
        $this->assertEquals('component 1', $this->service->getContent());
    }

}