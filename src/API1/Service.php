<?php
/**
 * Created by PhpStorm.
 * User: michaelsandritter
 * Date: 13/11/15
 * Time: 11:59
 */
namespace Komponente1\API1;

class Service {

    private $content;

    function __construct()
    {
        $this->content = 'component 1';
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}